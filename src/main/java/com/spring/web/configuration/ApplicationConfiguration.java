package com.spring.web.configuration;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class ApplicationConfiguration extends AbstractAnnotationConfigDispatcherServletInitializer {

    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ CoreConfiguration.class };
    }

    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{ WebConfiguration.class };
    }

    protected String[] getServletMappings() {
        return new String[]{"/v1/*"};
    }
}
