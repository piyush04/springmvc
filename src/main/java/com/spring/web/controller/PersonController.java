package com.spring.web.controller;

import com.spring.core.domain.Person;
import com.spring.core.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping
    public String welcome() {
        return "Root to the application";
    }

    @GetMapping(value = {"/get"})
    public @ResponseBody Person getPerson() {
        Person randomPerson = personService.getPerson();
        randomPerson.setName("Alice");
        randomPerson.setAge(30);
        return randomPerson;
    }
}
