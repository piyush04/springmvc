package com.spring.core.domain;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class Person {

    private String name;

    private UUID id;

    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
