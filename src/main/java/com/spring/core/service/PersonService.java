package com.spring.core.service;

import com.spring.core.dao.PersonDao;
import com.spring.core.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    @Autowired
    private PersonDao personDao;

    public Person getPerson() {
        return personDao.getPerson();
    }

}
