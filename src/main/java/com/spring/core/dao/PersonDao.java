package com.spring.core.dao;

import com.spring.core.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public class PersonDao {

    @Autowired
    private Person person;


    public Person getPerson() {
        person.setId(UUID.randomUUID());
        return person;
    }
}
